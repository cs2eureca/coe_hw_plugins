#include <realtime_tools/realtime_publisher.h>
#include <pluginlib/class_list_macros.h>
#include <std_msgs/Float64MultiArray.h>

#include <itia_futils/string.h>
#include <coe_core/coe_base.h>
#include <coe_core/ds301/coe_bitwise_struct.h>
#include <coe_core/ds301/coe_sdo_dictionary.h>
#include <coe_core/ds402/coe_xfsm_utilities.h>
#include <coe_core/ds402/coe_xfsm_symbols.h>
#include <coe_core/ds402/coe_bitwise_struct.h>

#include <coe_driver/diagnostic/coe_generic_analyzer.h>
#include <coe_driver/hw_plugin/coe_hw_base_plugin.h>

#include <coe_hw_plugins/coe_ds402_plugin.h>


namespace coe_hw_plugins
{

DS402Complete::DS402Complete( ) 
{ 
  
}

DS402Complete::~DS402Complete( ) 
{ 
  
}




///////////////////////////////////////
bool DS402Complete::initialize(ros::NodeHandle& nh, const std::string& device_coe_parameter, int address)
{
  if (!CoeHwPlugin::initialize(nh, device_coe_parameter, address) )
    return false;
  
  auto available_moo = coe_core::ds402::SUPPORTED_DRIVE_MODES();
  if(!readSdo(&available_moo) )
  {
    ROS_ERROR("Impossible to get the supported drive modes");
    return false;
  }
  std::bitset< 32 > smoo( available_moo.value() );
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_PROFILED_POSITION_MODE                           ] ) state_names_.push_back( coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_PROFILED_POSITION_MODE              ) );
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_VELOCITY_MODE                                    ] ) state_names_.push_back(  coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_VELOCITY_MODE                      ) );
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_PROFILED_VELOCITY_MODE                           ] ) state_names_.push_back(  coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_PROFILED_VELOCITY_MODE             ) );
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_PROFILED_TORQUE_MODE                             ] ) state_names_.push_back(  coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_PROFILED_TORQUE_MODE               ) );
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_RESERVED                                         ] ) ;
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_HOMING_MODE                                      ] ) state_names_.push_back(  coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_HOMING_MODE                        ) );
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_INTERPOLATED_POSITION_MODE                       ] ) state_names_.push_back(  coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_INTERPOLATED_POSITION_MODE         ) );
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_CYCLIC_SYNCHRONOUS_POSITION_MODE                 ] ) state_names_.push_back(  coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_POSITION_MODE   ) );
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_CYCLIC_SYNCHRONOUS_VELOCITY_MODE                 ] ) state_names_.push_back(  coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_VELOCITY_MODE   ) );
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_CYCLIC_SYNCHRONOUS_TORQUE_MODE                   ] ) state_names_.push_back(  coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_TORQUE_MODE     ) );
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_CYCLIC_SYNCHRONOUS_TORQUE_WITH_COMMUTATION_ANGLE ] ) ;

  {
    ROS_WARN("[ %s%s%s ] Check the Tx mandatory objects [%sRUNNING%s]",BOLDCYAN, module_->getIdentifier().c_str(), RESET, BOLDMAGENTA,RESET);
    std::vector< std::string > key( std::begin( TxPdoMandatory::Key   ), std::end(TxPdoMandatory::Key) );
    std::vector< uint32_t >    idx( std::begin( TxPdoMandatory::Index ), std::end(TxPdoMandatory::Index) );
    for( size_t j=0; j<key.size(); j++ )
    {
      auto const & idx_ = idx[j];
      auto const & key_ = key[j];

      if( module_->getTxPdo ( ).find( idx_ ) == module_->getTxPdo( ).end() )
      {
        ROS_ERROR("[ %s%s%s ] The field '%s/%x' is not in the PDO list", BOLDCYAN, module_->getIdentifier().c_str(), RESET, key_.c_str(),  idx_);
        ROS_INFO ("[ %s%s%s ] PDOs mapped: \n%s"                       , BOLDCYAN, module_->getIdentifier().c_str(), RESET, module_->getTxPdo ( ).to_string().c_str() );
        ROS_ERROR("[ %s%s%s ] Abort."                                  , BOLDCYAN, module_->getIdentifier().c_str(), RESET );
        return false;
      }
    }
    ROS_DEBUG_STREAM( module_->getTxPdo ( ).to_string() );
    ROS_INFO("[ %s%s%s ] Check the Tx mandatory objects [%sOK%s]",BOLDCYAN,module_->getIdentifier().c_str(), RESET, BOLDGREEN, RESET); 
  }

  {
    ROS_INFO("[ %s%s%s ] Check the Rx mandatory objects [%sRUNNING%s]",BOLDCYAN, module_->getIdentifier().c_str(), RESET,BOLDMAGENTA,RESET);
    std::vector< std::string > key( std::begin( RxPdoMandatory::Key ), std::end( RxPdoMandatory::Key ) );
    std::vector< uint32_t >    idx( std::begin( RxPdoMandatory::Index ), std::end( RxPdoMandatory::Index ) );
    for( size_t j=0; j<key.size(); j++ )
    {
      auto const & idx_ = idx[j];
      auto const & key_ = key[j];
      if( module_->getRxPdo ( ).find( idx_ ) == module_->getRxPdo ( ).end() )
      {
        ROS_ERROR("[ %s%s%s ] The field '%s/%x' is not in the PDO list" ,BOLDCYAN, module_->getIdentifier().c_str(), RESET, key_.c_str(),  idx_);        
        ROS_INFO ("[ %s%s%s ] PDOs mapped: \n%s"                        ,BOLDCYAN, module_->getIdentifier().c_str(), RESET, module_->getRxPdo ( ).to_string().c_str() );
        ROS_ERROR("[ %s%s%s ] Abort."                                   ,BOLDCYAN, module_->getIdentifier().c_str(), RESET);
        return false;
      }
    }
    ROS_DEBUG_STREAM( module_->getRxPdo ( ).to_string() );
    ROS_INFO("[ %s%s%s ] Check the Rx mandatory objects [%sOK%s]",BOLDCYAN, module_->getIdentifier().c_str(), RESET, BOLDGREEN,RESET); 
  }
  
  //Pointer to 
  ROS_INFO("[ %s%s%s ] has analog inputs? %s",BOLDCYAN, module_->getIdentifier().c_str(), RESET, ( module_->getAnalogInputs( ).size() ? "YES" : "NO" ) );
  for( auto const & a_i : module_->getAnalogInputs  ( ) ) analog_inputs_  [ a_i.first ] = 0.0;
  
  ROS_INFO("[ %s%s%s ] has analog outputs? %s",BOLDCYAN, module_->getIdentifier().c_str(), RESET, ( module_->getAnalogOutputs( ).size() ? "YES" : "NO" ) );
  for( auto const & a_o : module_->getAnalogOutputs ( ) ) analog_outputs_ [ a_o.first ] = 0.0;
  
  ROS_INFO("[ %s%s%s ] has digital inputs? %s",BOLDCYAN, module_->getIdentifier().c_str(), RESET, ( module_->getDigitalInputs( ).size() ? "YES" : "NO" ) );
  for( auto const & d_i : module_->getDigitalInputs ( ) ) digital_inputs_ [ d_i.first ] = true;
  
  ROS_INFO("[ %s%s%s ] has digital outputs? %s",BOLDCYAN, module_->getIdentifier().c_str(), RESET, ( module_->getDigitalOutputs( ).size() ? "YES" : "NO" ) );
  for( auto const & d_o : module_->getDigitalOutputs( ) ) digital_outputs_[ d_o.first ] = false;

 
  statusid_prev_ = coe_core::ds402::STATE_NO_STATE;
  status_word_  = static_cast< coe_core::DataObjectEntry<uint16_t>* >( ( *(module_->getTxPdo().find( TxPdoMandatory::Index[ TxPdoMandatory::STATUS_WORD  ] ) ) ) .get()  ) ;
  control_word_ = static_cast< coe_core::DataObjectEntry<uint16_t>* >( ( *(module_->getRxPdo().find( RxPdoMandatory::Index[ RxPdoMandatory::CONTROL_WORD ] ) ) ) .get()  );

  // Init values
  read();     // assigend:   feedback_position_, feedback_velocity_  = 0.0, feedback_torque_    = 0.0, 
  
  command_position_   = feedback_position_;
  command_velocity_   = 0.0;
  command_torque_     = 0.0;
  setControlWord( coe_core::ds402::to_control_word(0x0) );

  // Init support objs
  operation_mode_     = coe_core::ds402::MOO_NO_MODE;
  switching_operation_mode_ = false;
  
  return true;
}

coe_core::ds402::ModeOperationID  DS402Complete::getOperationMode ( ) 
{
  auto sdo = coe_core::ds402::MODES_OF_OPERATION_DISPLAY( );
  bool res = readSdo( &sdo );
  if ( !res ) 
  {
    throw std::runtime_error( "Error in gettin the MODES_OF_OPERATION_DISPLAY"  );
  }
  return coe_core::ds402::to_modeofoperationid( sdo.value() ) ;
}

bool DS402Complete::setOperationMode(const coe_core::ds402::ModeOperationID moo, const uint8_t max_num_trials)
{
  ros::WallRate rt( 1. / ( module_->getLoopRateDecimation() * operational_time_ ) );
  
  uint8_t trial = 0;
  do
  {
    read();
    // nothing to do so far
    write();
    rt.sleep();
  } while( trial++ < 100 );
  
  
  trial = 0;
  ROS_WARN("Change the DS402 State to STATE_READY_TO_SWITCH_ON");
  do
  {
    read();
    
    coe_core::ds402::StateID        actl_state    = coe_core::ds402::to_stateid ( getStatusWord() );
    ROS_INFO("[ %s ] Actual State: %s", module_->getIdentifier().c_str(), coe_core::ds402::to_string( actl_state ).c_str() );
    coe_core::ds402::control_word_t control_word  = getControlWord();
    switch( actl_state )
    {
      case coe_core::ds402::STATE_FAULT             : coe_core::ds402::to_control_word(coe_core::ds402::CommandID::CMD_FAULT_RESET, (uint16_t*)&control_word); break;
      case coe_core::ds402::STATE_SWITCH_ON_DISABLED: coe_core::ds402::to_control_word(coe_core::ds402::CommandID::CMD_SHUTDOWN,    (uint16_t*)&control_word); break;
      case coe_core::ds402::STATE_READY_TO_SWITCH_ON: coe_core::ds402::to_control_word(coe_core::ds402::CommandID::CMD_LOOPBACK,    (uint16_t*)&control_word); break;
      case coe_core::ds402::STATE_SWITCHED_ON       : coe_core::ds402::to_control_word(coe_core::ds402::CommandID::CMD_SHUTDOWN,    (uint16_t*)&control_word); break;
      case coe_core::ds402::STATE_OPERATION_ENABLED : coe_core::ds402::to_control_word(coe_core::ds402::CommandID::CMD_DISABLE,     (uint16_t*)&control_word); break;
    }
    
    setControlWord(control_word);    
    
    write();
    rt.sleep();
    actl_state  = coe_core::ds402::to_stateid ( getStatusWord() ); 
    write();
    
    if( actl_state == coe_core::ds402::STATE_READY_TO_SWITCH_ON )
    {
      ROS_WARN("Module in STATE_READY_TO_SWITCH_ON");
      break;
    } 
    else if( trial++ > max_num_trials )
    {
      ROS_ERROR("[ %s ] Actual State: %s", module_->getIdentifier().c_str(), coe_core::ds402::to_string( actl_state ).c_str() );
      ROS_ERROR("[ %s ] Aborted in disabling, n. trials %d. Try manually.\n",  module_->getIdentifier().c_str(), trial);
      return false;
    }
    
  } while( 1 );
  

  trial = 0;
  ROS_WARN("Change the operational requested mode '%s'", coe_core::ds402::to_string( moo ).c_str() );
  do 
  {
    read();
    if ( trial++ > max_num_trials ) 
    {
      ROS_ERROR("Aborted, n. trials %d (device: %s)\n",  trial, module_->getIdentifier().c_str() );
      return false;
    }

    //TODO put it in a parallel thread!
    auto sdo = coe_core::ds402::MODES_OF_OPERATION( moo );
    if ( !writeSdo( &sdo ) ) 
    {
      ROS_ERROR("asked profile velocity mode %d (device: %s)\n", coe_core::ds402::MOO_PROFILED_VELOCITY_MODE, module_->getIdentifier().c_str());
      return false;
    }
    write();

  } while( (operation_mode_ = getOperationMode( ) ) != moo ) ;
  
  
  ROS_WARN("Change the DS402 State to STATE_OPERATION_ENABLED");
  trial = 0;
   do
  {
    read();
    
    coe_core::ds402::StateID        actl_state    = coe_core::ds402::to_stateid ( getStatusWord() );
    coe_core::ds402::control_word_t control_word  = getControlWord();
    switch( actl_state )
    {
      case coe_core::ds402::STATE_FAULT             : coe_core::ds402::to_control_word(coe_core::ds402::CommandID::CMD_FAULT_RESET, (uint16_t*)&control_word); break;
      case coe_core::ds402::STATE_SWITCH_ON_DISABLED: coe_core::ds402::to_control_word(coe_core::ds402::CommandID::CMD_SHUTDOWN,    (uint16_t*)&control_word); break;
      case coe_core::ds402::STATE_READY_TO_SWITCH_ON: coe_core::ds402::to_control_word(coe_core::ds402::CommandID::CMD_SWITCH_ON,   (uint16_t*)&control_word); break;
      case coe_core::ds402::STATE_SWITCHED_ON       : coe_core::ds402::to_control_word(coe_core::ds402::CommandID::CMD_ENABLE,      (uint16_t*)&control_word); break;
      case coe_core::ds402::STATE_OPERATION_ENABLED : coe_core::ds402::to_control_word(coe_core::ds402::CommandID::CMD_LOOPBACK,    (uint16_t*)&control_word); break;
    }
    
    setControlWord(control_word);    
    
    write();
    
    rt.sleep();
    actl_state  = coe_core::ds402::to_stateid ( getStatusWord() ); 
    write();
    
    if( actl_state == coe_core::ds402::STATE_OPERATION_ENABLED )
    {
      ROS_WARN("Module in STATE_OPERATION_ENABLED");
      break;
    } 
    else if( trial++ > max_num_trials )
    {
      ROS_ERROR("Aborted in disabling, n. trials %d (device: %s). Try manually.\n",  trial, module_->getIdentifier().c_str() );
      return false;
    }
  } while( 1 );
  
  return  true;
}


coe_core::ds402::ModeOperationID  DS402Complete::getOperationModeID( ) 
{
  auto sdo = coe_core::ds402::MODES_OF_OPERATION_DISPLAY( );
  if ( !readSdo( &sdo) ) throw std::runtime_error( "Error in gettin the MODES_OF_OPERATION_DISPLAY"  );

  return coe_core::ds402::to_modeofoperationid( sdo.value() );
}



std::vector<std::string>   DS402Complete::getStateNames ( ) const 
{ 
  return state_names_; 
}

bool DS402Complete::checkErrorsActive ( ) 
{ 
  std::lock_guard<std::mutex> lock(read_mtx_);  
  return  ( switching_operation_mode_  )                         ? false
        : ( operation_mode_ == coe_core::ds402::MOO_NO_MODE )    ? false
        : ( coe_core::ds402::to_stateid( status_word_->value() ) != coe_core::ds402::STATE_OPERATION_ENABLED);

  // ( coe_core::ds402::to_stateid( status_word_->value() ) == coe_core::ds402::STATE_FAULT); 
}
  
std::string  DS402Complete::getActualState ( )  
{ 
  return coe_core::ds402::MODEOPERATIONSID_STRINGS.at( operation_mode_  ); 
}

coe_core::ds402::status_word_t  DS402Complete::getStatusWord  ( )  
{ 
  coe_core::ds402::status_word_t ret;
  std::lock_guard<std::mutex> lock(read_mtx_);  
  ret = coe_core::ds402::to_status_word( status_word_->value() ); 
  return ret; 
}

coe_core::ds402::control_word_t  DS402Complete::getControlWord  ( )  
{ 
  coe_core::ds402::control_word_t ret;
  std::lock_guard<std::mutex> lock(read_mtx_);  
  ret = coe_core::ds402::to_control_word( control_word_->value() ); 
  return ret; 
}


std::vector<std::string> DS402Complete::getAnalogInputNames ( ) const 
{ 
  std::vector<std::string> n; 
  if( n.size() != module_->getAnalogInputs().size()  )  
    for( auto const & m : module_->getAnalogInputs()   ) 
      n.push_back(m.first); 
  return n; 
}

std::vector<std::string> DS402Complete::getAnalogOutputNames ( ) const 
{ 
  std::vector<std::string> n; 
  for( auto const & m : module_->getAnalogOutputs()  ) 
    n.push_back(m.first); 
  return n; 
}

std::vector<std::string>   DS402Complete::getDigitalInputNames ( ) const 
{ 
  std::vector<std::string> n; 
  for( auto const & m : module_->getAnalogInputs()   ) 
    n.push_back(m.first); 
  return n; 
}
std::vector<std::string>   DS402Complete::getDigitalOutputNames ( ) const 
{ 
  std::vector<std::string> n; 
  for( auto const & m : module_->getDigitalOutputs() ) 
    n.push_back(m.first); 
  return n; 
}
uint16_t* DS402Complete::stateHandle ( )
{
  return (uint16_t*)status_word_->data();
}
uint16_t* DS402Complete::controlWordHandle( )
{
  return (uint16_t*)control_word_->data();
}

void  DS402Complete::jointStateHandle   (double** pos, double** vel, double** eff) 
{
  *pos = &feedback_position_;
  *vel = &feedback_velocity_;
  *eff = &feedback_torque_;

}
void  DS402Complete::jointCommandHandle (double** pos, double** vel, double** eff)  
{
  *pos = &command_position_;
  *vel = &command_velocity_;
  *eff = &command_torque_;
}

double* DS402Complete::analogInputValueHandle  (const std::string& name) 
{
  double* ret = nullptr;
  try
  {
    ret = &( analog_inputs_[name] );
  }
  catch(std::exception& e)
  {
    ROS_FATAL("Error in %s: %s", __FUNCTION__, e.what() );
    ret = nullptr;
  }
  return ret;
}
double*  DS402Complete::analogOutputValueHandle (const std::string& name) 
{
  double* ret = nullptr;
  try
  {
  
  ret = & ( analog_outputs_[ name ] );
  }
  catch(std::exception& e)
  {
    ROS_FATAL("Error in %s: %s", __FUNCTION__, e.what() );
    ret = nullptr;
  }
  return ret;
}

bool* DS402Complete::digitalInputValueHandle  (const std::string& name) 
{
  
  bool* ret = nullptr;
  try
  {
    ret = &  ( digital_inputs_[ name ] );
  }
  catch(std::exception& e)
  {
    ROS_FATAL("Error in %s: %s", __FUNCTION__, e.what() );
    ret = nullptr;
  }
  return ret;

}
bool*  DS402Complete::digitalOutputValueHandle (const std::string& name)  
{
  bool* ret=nullptr;
  try
  {
    ret = & ( digital_outputs_[ name ] );
  }
  catch(std::exception& e)
  {
    ROS_FATAL("Error in %s: %s", __FUNCTION__, e.what() );
    ret=nullptr;
  }
  return ret;
}

void DS402Complete::getErrors ( std::vector< std::pair< std::string,std::string> >& ret )
{ 
  ret.clear();

  auto eregister      = coe_core::ds301::ERROR_REGISTER();
  auto emanufacturer  = coe_core::ds301::MANUFACTURER_STATUS_REGISTER( );
  auto epredef        = coe_core::ds301::PREDEFINED_ERROR_FIELD_N();

  if( readSdo( &eregister) ) 
  {
    coe_core::ds301::error_register_t error_register; 
    if( eregister.value() )
      ret.push_back( std::make_pair( coe_core::to_string_hex( eregister.value() ), coe_core::ds301::to_string( error_register << eregister.value() ) ) );
  }

  if( readSdo(&emanufacturer) )
  {
    coe_core::ds301::manufacturer_status_register_t  manufacturer_status_register;
    if( emanufacturer.value() )
      ret.push_back( std::make_pair( coe_core::to_string_hex( emanufacturer.value() ), coe_core::ds301::to_string( manufacturer_status_register << emanufacturer.value() ) ) );
  }

  if( readSdo(&epredef) )
  {
    for( size_t i=1; i<= epredef.value(); i++) 
    {
      uint32_t val = 0;
      coe_core::DataObjectEntry<uint32_t> error_field( 0x1003, i, "Predefined Error Field", val );
      if( readSdo(&error_field) )
      {
          coe_core::ds301::predefined_error_field_t predefined_error_field;
          ret.push_back( std::make_pair( coe_core::to_string_hex(error_field.value()), coe_core::ds301::to_string( predefined_error_field << error_field.value() ) ) );
      }
    }
  }
  
  coe_core::ds402::status_word_t s = getStatusWord();
  ret.push_back( std::make_pair( "Status Word", coe_core::ds402::to_string(s,'s') ) );
}

bool DS402Complete::setTargetState( const std::string& state_name )
{
  bool ret = false;
  switching_operation_mode_ = true;
  coe_core::ds402::ModeOperationID moo = coe_core::ds402::to_modeofoperationid(state_name);
  switch( moo )
  {
    case coe_core::ds402::MOO_HOMING_MODE                       : ret = setOperationMode( coe_core::ds402::MOO_HOMING_MODE );                     break;
    case coe_core::ds402::MOO_PROFILED_POSITION_MODE            : throw std::runtime_error(( "'"+ state_name + "' Not yet suported").c_str() );   break;
    case coe_core::ds402::MOO_PROFILED_VELOCITY_MODE            : throw std::runtime_error(( "'"+ state_name + "' Not yet suported").c_str() );   break;
    case coe_core::ds402::MOO_PROFILED_TORQUE_MODE              : throw std::runtime_error(( "'"+ state_name + "' Not yet suported").c_str() );   break;
    case coe_core::ds402::MOO_INTERPOLATED_POSITION_MODE        : throw std::runtime_error(( "'"+ state_name + "' Not yet suported").c_str() );   break;
    case coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_POSITION_MODE  : ret = setOperationMode( coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_POSITION_MODE ); break;
    case coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_VELOCITY_MODE  : ret = setOperationMode( coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_VELOCITY_MODE ); break;
    case coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_TORQUE_MODE    : ret = setOperationMode( coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_TORQUE_MODE   ); break;
  }
  switching_operation_mode_ = !ret;
  return ret;
}

bool DS402Complete::setControlWord( const coe_core::ds402::control_word_t& control_word )
{
  (*control_word_) << (uint8_t*)&control_word;
  return true;
}


bool DS402Complete::read()
{
  coe_core::ds402::StateID stateid = coe_core::ds402::STATE_NO_STATE;
  bool                     state_ok = false;
  try 
  {
    if( !CoeHwPlugin::read() )
      return false;
    
    
    std::lock_guard<std::mutex> lock(read_mtx_);  
    auto & position =  module_->getAxisFeedback( TxPdoMandatory::Key[TxPdoMandatory::POSITION ] );
    feedback_position_ = position.entry->get<int32_t>( ) * position.scale +  position.offset;
    
    auto & velocity =  module_->getAxisFeedback( TxPdoMandatory::Key[TxPdoMandatory::VELOCITY ]);
    feedback_velocity_ = velocity.entry->get<int32_t>( ) * velocity.scale +  velocity.offset;

    auto & torque =  module_->getAxisFeedback( TxPdoMandatory::Key[TxPdoMandatory::TORQUE] );
    feedback_torque_ = torque.entry->get<int16_t>( ) * torque.scale +  torque.offset;

    for( auto & a_i  :  module_->getAnalogInputs(  ) )
    { 
      analog_inputs_[ a_i.first ] = a_i.second.entry->get<int32_t>( ) * a_i.second.scale +  a_i.second.offset;
    }

    for( auto & d_i  :  module_->getDigitalInputs(  ) )
    {
      std::bitset< 32 > bits( d_i.second.entry->get<uint32_t>( ) ); 
      digital_inputs_[ d_i.first ] = bits[ d_i.second.bit ];
    }
    
    stateid         = coe_core::ds402::to_stateid( status_word_->value() );
    if( stateid != statusid_prev_ )
    {
      ROS_DEBUG_STREAM( coe_core::ds402::what_is_happen(statusid_prev_,stateid,1)); 
    }
    statusid_prev_  = stateid;
    
    
    switch( operation_mode_ )
    {
      case coe_core::ds402::MOO_HOMING_MODE:
      {
        command_position_ = feedback_position_;
        command_torque_   = 0.0;
        command_velocity_ = 0.0;
      } 
      break;
      case coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_POSITION_MODE:
      {
        if( stateid == coe_core::ds402::STATE_OPERATION_ENABLED) 
        {
          coe_core::ds402::cyclic_pos_status_word_t*  sw_ = (coe_core::ds402::cyclic_pos_status_word_t*)status_word_->data();
        
          if( sw_->bits.internal_limit_active )
            ROS_WARN_THROTTLE(2,"[ %s%s%s%s ] Internal limit active ",  BOLDCYAN, module_->getIdentifier().c_str(), RESET,YELLOW );
          
          if( sw_->bits.target_reached)
            ROS_WARN_THROTTLE(2,"[ %s%s%s%s ] Target reached",  BOLDCYAN, module_->getIdentifier().c_str(), RESET,YELLOW );
          
        }
      }
      break;
    }
  }
  catch( std::exception& e )
  {
    ROS_ERROR("exception caught in READ: \n%s\n",e.what());
    return false;
  }
  
  return !checkErrorsActive();
}
bool DS402Complete::write()
{
  bool ret = false;
  try 
  {
    auto & position =  module_->getAxisCommand( RxPdoMandatory::Key[RxPdoMandatory::POSITION ] );
    position.entry->set<int32_t>( ( command_position_ -  position.offset ) / position.scale );
    
    auto & velocity =  module_->getAxisCommand( RxPdoMandatory::Key[RxPdoMandatory::VELOCITY ] );
    velocity.entry->set<int32_t>( ( command_velocity_ -  velocity.offset ) / velocity.scale );

    auto & torque =  module_->getAxisCommand( RxPdoMandatory::Key[RxPdoMandatory::TORQUE ] );
    torque.entry->set<int16_t>( ( command_torque_ -  torque.offset ) / torque.scale );

    for( auto & a_i  :  module_->getAnalogOutputs(  ) )
    { 
      a_i.second.entry->set<int32_t>( ( analog_outputs_[ a_i.first ] - a_i.second.offset ) / a_i.second.scale );
    }
    
    if( module_->getDigitalOutputs().size() )
    {
      std::bitset< 32 > bits( 0x0 ); 
      for( auto & d_i  :  module_->getDigitalOutputs(  ) )
      {
        bits[ d_i.second.bit ] = digital_outputs_[ d_i.first ];
      }
      module_->getDigitalOutputs().begin()->second.entry->set<uint32_t>( bits.to_ulong() ) ;
    }

    switch( operation_mode_ )
    {
      case coe_core::ds402::MOO_HOMING_MODE:
      {
        if( coe_core::ds402::to_stateid( status_word_->value() ) == coe_core::ds402::STATE_OPERATION_ENABLED) 
        {
          
          coe_core::ds402::homing_status_word_t*  sw_ = (coe_core::ds402::homing_status_word_t* )status_word_->data();
          coe_core::ds402::homing_control_word_t* cw_ = (coe_core::ds402::homing_control_word_t*)control_word_->data();
          if( sw_->bits.homing_error == 1 ) 
          {
            cw_->bits.home_operation_start = 0;
            cw_->bits.halt = 1;
            ROS_ERROR( "**** **** ERROR IN HOMING FROM status word: %s ******", coe_core::ds402::to_string ( *sw_, 'b' ).c_str() );    
          } 
          else if( sw_->bits.homing_attained && sw_->bits.target_reached ) 
          {
            cw_->bits.home_operation_start = 0;
            cw_->bits.halt = 0;
      //       homing_pos = rpdo_trombone.position_actual_value;
          } 
          else 
          {
            cw_->bits.home_operation_start = 1;
            cw_->bits.halt                 = 0;
          }
        }
      } 
      break;
      case coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_POSITION_MODE:
      {
        if( coe_core::ds402::to_stateid( status_word_->value() ) == coe_core::ds402::STATE_OPERATION_ENABLED) 
        {
          coe_core::ds402::cyclic_pos_control_word_t*  cw_ = (coe_core::ds402::cyclic_pos_control_word_t*)control_word_->data();
        
          cw_->bits.operation_specific = 7;
          cw_->bits.halt = 0;
        }
      }
      break;
    }

    ret = CoeHwPlugin::write();
  }
  catch( std::exception& e )
  {
    ROS_ERROR("exception caught in READ: \n%s\n",e.what());
    return false;
  }
  
  return ret;
}

///////////////////////////////////////
PLUGINLIB_EXPORT_CLASS(coe_hw_plugins::DS402Complete, coe_driver::CoeHwPlugin );


}
