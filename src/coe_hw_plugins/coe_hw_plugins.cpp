#include <ros/ros.h>

#include <coe_hw_plugins/coe_basehw_plugin.h>


namespace coe_hw_plugins
{
  
bool CoeHwPlugin::initialize(ros::NodeHandle& nh, const std::string& device_coe_parameter)
{
  XmlRpc::XmlRpcValue config;
  if( !nh.getParam(device_coe_parameter, config ) )
  {
    ROS_ERROR("Error in extracting data from '%s/%s'", nh.getNamespace().c_str(), device_coe_parameter.c_str() );
    return false;
  }
  module_.reset( new coe_driver::ModuleDescriptor( config ) );
  
  // TODO check correctness throu network_
  
  set_sdo_  = nh.serviceClient<coe_driver::SetSdo>(SET_SDO_SERVER_NAMESPACE);
  get_sdo_  = nh.serviceClient<coe_driver::GetSdo>(GET_SDO_SERVER_NAMESPACE);
  ROS_INFO("Creating the shared memory object for '%s'", module_->getIdentifier().c_str() );
  pdo_shared_memory_.reset( new coe_driver::ModuleSharedMemory( module_->getIdentifier() ) );
  
  ROS_INFO("Allocate the shared memory object for '%s'", module_->getIdentifier().c_str() );
  
  prxpdo_          = new uint8_t[ pdo_shared_memory_->rx_pdo_.getSize() ];
  ptxpdo_          = new uint8_t[ pdo_shared_memory_->tx_pdo_.getSize() ];
  prxpdo_previous_ = new uint8_t[ pdo_shared_memory_->rx_pdo_.getSize() ];
  ptxpdo_previous_ = new uint8_t[ pdo_shared_memory_->tx_pdo_.getSize() ];
  
  return true;
}


bool CoeHwPlugin::read() 
{
  try
  {
    assert( pdo_shared_memory_->tx_pdo_.getSize() > 0 );
    assert( ptxpdo_previous_ != NULL );
    
    std::memcpy( ptxpdo_previous_, ptxpdo_, pdo_shared_memory_->tx_pdo_.getSize() ); //getSize consider the prepended time
    
    pdo_shared_memory_->tx_pdo_  >> &ptxpdo_[0];   // operator >> loses the "time" label
    
    module_->updateInputs(&ptxpdo_[0], false);
  }
  catch( std::exception& e )
  {
    ROS_ERROR("READ EXCEPTION: %s",e.what());
    return false;
  }
  return true;
}

bool CoeHwPlugin::write() 
{
  try
  {
    assert( pdo_shared_memory_->rx_pdo_.getSize() > 0 );
    assert( prxpdo_previous_ != NULL );
    
    std::memcpy( prxpdo_previous_, prxpdo_, pdo_shared_memory_->rx_pdo_.getSize( false ) );   
    std::memcpy( prxpdo_, module_->getRxPdo().data(false),  pdo_shared_memory_->rx_pdo_.getSize( false ) );
    
//     std::cout << module_->getRxPdo().getPdoEntry(0x160F).obj().front()->to_string() << std::endl;
//     printf("ctrl word: %hx, trg tau: %hd trg pos: %d vel offset % tau off %hd trg vel %d dig %x"
//       , *(uint16_t*)&prxpdo_[0]
//       , *( int16_t*)&prxpdo_[2]
//       , *( int32_t*)&prxpdo_[4]
//       , *( int32_t*)&prxpdo_[8]
//       , *( int16_t*)&prxpdo_[12]
//       , *( int32_t*)&prxpdo_[14]
//       , *( uint32_t*)&prxpdo_[16]);     // 2

    //     std::cout << "size:: "  <<  pdo_shared_memory_->rx_pdo_.getSize(false) << std::endl;      // 2
//     std::cout << "ctrl word: " << coe_core::to_string_hex<uint16_t>(*(uint16_t*)&prxpdo_[0]) << std::endl;      // 2
//     std::cout << "trg   tau: " << coe_core::to_string_hex<int16_t >(*( int16_t*)&prxpdo_[2]) << std::endl;      // 2
//     std::cout << "trg   pos: " << coe_core::to_string_hex<int32_t >(*( int32_t*)&prxpdo_[4]) << std::endl;      // 4
//     std::cout << "vel   off: " << coe_core::to_string_hex<int32_t >(*( int32_t*)&prxpdo_[8]) << std::endl;      // 4
//     std::cout << "tau   off: " << coe_core::to_string_hex<int16_t >(*( int16_t*)&prxpdo_[12]) << std::endl;     // 2
//     std::cout << "trg   vel: " << coe_core::to_string_hex<int32_t >(*( int32_t*)&prxpdo_[14]) << std::endl;     // 2
//     std::cout << "dig      : " << coe_core::to_string_hex<uint32_t>(*( uint32_t*)&prxpdo_[16]) << std::endl;     // 2
    
    pdo_shared_memory_->rx_pdo_ <<  &prxpdo_[0];  // Here the time is prepended automatically!!!!
    
  }
  catch( std::exception& e )
  {
    ROS_ERROR("WRITE EXCEPTION: %s",e.what());
    return false;
  }
  return true;
}
      
      
  
bool CoeHwPlugin::readSdo ( coe_core::BaseDataObjectEntry* in)
{
  coe_driver::GetSdo::Request req;
  coe_driver::GetSdo::Response res;
  
  req.index     = in->index();
  req.subindex  = in->subindex();
  req.sdotype   = in->type() == ECT_UNSIGNED8  ? coe_driver::SetSdo::Request::TYPE_U8
                : in->type() == ECT_UNSIGNED16 ? coe_driver::SetSdo::Request::TYPE_U16
                : in->type() == ECT_UNSIGNED32 ? coe_driver::SetSdo::Request::TYPE_U32
                : in->type() == ECT_UNSIGNED64 ? coe_driver::SetSdo::Request::TYPE_U64
                : in->type() == ECT_INTEGER8   ? coe_driver::SetSdo::Request::TYPE_I8
                : in->type() == ECT_INTEGER16  ? coe_driver::SetSdo::Request::TYPE_I16
                : in->type() == ECT_INTEGER32  ? coe_driver::SetSdo::Request::TYPE_I32
                : in->type() == ECT_INTEGER64  ? coe_driver::SetSdo::Request::TYPE_I64
                : 99;

  assert( req.sdotype != 99 );
  
  req.desc      = in->name();
  req.module_id = module_->getIdentifier();
  req.timeout   = 0.1;
  
  if( !set_sdo_.exists() )
  {
    ROS_FATAL("Error. The service '%s' is not available", set_sdo_.getService().c_str() );
    return false;
  }
  
  if( !get_sdo_.call(req,res) )
  {
    ROS_FATAL("Error. Server does not answer to the call. ");
    return false;
  }
  if( !res.success )
  {
    ROS_FATAL("Error. Getting the sdo '%x:%u' failed. ", in->index(), in->subindex() );
    return false;
  }
  in->set( res.value );
  return true;
}
    
bool CoeHwPlugin::writeSdo ( const coe_core::BaseDataObjectEntry* in) 
{
  ROS_INFO_STREAM( "[" << BOLDMAGENTA << " PROCESS "<< RESET << "] Set COB-ID: " << in->to_string() );
  coe_driver::SetSdo::Request req;
  coe_driver::SetSdo::Response res;
  
  req.index     = in->index();
  req.subindex  = in->subindex();

  req.sdotype   = in->type() == ECT_UNSIGNED8  ? coe_driver::SetSdo::Request::TYPE_U8
                : in->type() == ECT_UNSIGNED16 ? coe_driver::SetSdo::Request::TYPE_U16
                : in->type() == ECT_UNSIGNED32 ? coe_driver::SetSdo::Request::TYPE_U32
                : in->type() == ECT_UNSIGNED64 ? coe_driver::SetSdo::Request::TYPE_U64
                : in->type() == ECT_INTEGER8   ? coe_driver::SetSdo::Request::TYPE_I8
                : in->type() == ECT_INTEGER16  ? coe_driver::SetSdo::Request::TYPE_I16
                : in->type() == ECT_INTEGER32  ? coe_driver::SetSdo::Request::TYPE_I32
                : in->type() == ECT_INTEGER64  ? coe_driver::SetSdo::Request::TYPE_I64
                : 99;

  assert( req.sdotype != 99 );
  
  in->get( req.value );     
  
  req.desc      = in->name();
  
  req.module_id = module_->getIdentifier();
  
  req.timeout   = 0.1;
  
  if( !set_sdo_.exists() )
  {
    ROS_FATAL("Error. The service '%s' is not available", set_sdo_.getService().c_str() );
    return false;
  }
  
  if( !set_sdo_.call(req,res) )
  {
    ROS_FATAL("Error. Server does not asnwer to the call. ");
    return false;
  }
  if( !res.success )
  {
    ROS_INFO_STREAM( "[" << BOLDRED << "  FAILED "<< RESET << "] Set COB-ID: " << in->to_string() );
    return false;
  }
  ROS_INFO_STREAM( "[" << BOLDGREEN << "   OK    "<< RESET << "] Set COB-ID: " << in->to_string() );
  return true;
}
  
}