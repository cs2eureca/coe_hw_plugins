#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <std_srvs/Trigger.h>
#include <pluginlib/class_loader.h>
#include <realtime_tools/realtime_publisher.h>
#include <coe_hw_plugins/CoeMessage.h>
#include <coe_hw_plugins/CoeErrors.h>
#include <coe_hw_plugins/coe_basehw_plugin.h>

class TestPlugin
{
private:
  double* pos_msr, *vel_msr, *eff_msr;
  double* pos_cmd, *vel_cmd, *eff_cmd;
  
  pluginlib::ClassLoader<coe_hw_plugins::CoeHwPlugin> loader_;
  boost::shared_ptr<coe_hw_plugins::CoeHwPlugin> ds402plugin_;
  
public:
  TestPlugin( ros::NodeHandle& nh ) : loader_( "coe_hw_plugins", "coe_hw_plugins::CoeHwPlugin" )
  {
    try
    {
        ds402plugin_ = loader_.createInstance ( "coe_hw_plugins::DS402Complete" );
        ds402plugin_->initialize ( nh, "/coe_driver_node/coe/ElmoGoldWhistle1" );
        ds402plugin_->getJointStatePtr(pos_msr,vel_msr,eff_msr);
        ds402plugin_->getJointCommandPtr( pos_cmd, vel_cmd, eff_cmd);
    }
    catch ( pluginlib::PluginlibException& ex )
    {
        ROS_ERROR ( "The plugin failed to load for some reason. Error: %s", ex.what() );
    }

  }
  ~TestPlugin() 
  {
  }
  
  bool getCoeState( std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res )
  {
    std::string moo_str = ds402plugin_->getActualState( );
    coe_core::ds402::ModeOperationID  moo = coe_core::ds402::to_modeofoperationid( moo_str );
    res.message  += "Mode of Operation Active: " + std::to_string( moo ) + " - " + coe_core::ds402::to_string(moo) + "\n";
    if( moo == coe_core::ds402::MOO_HOMING_MODE )
    {
      coe_core::ds402::homing_status_word_t status_word; 
      res.message  += coe_core::ds402::to_string(status_word << *(uint16_t*)&( ds402plugin_->getStatusWord() ), 's' );
      
      coe_core::ds402::homing_control_word_t control_word; 
      res.message  += coe_core::ds402::to_string(control_word << *(uint16_t*)&( ds402plugin_->getControlWord() ), 's' );
    }
    else if( moo == coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_POSITION_MODE )
    {
      coe_core::ds402::cyclic_pos_status_word_t status_word; 
      res.message  += coe_core::ds402::to_string(status_word << *(uint16_t*)&( ds402plugin_->getStatusWord() ), 's' );
      
      coe_core::ds402::cyclic_pos_control_word_t control_word; 
      res.message  += coe_core::ds402::to_string(control_word << *(uint16_t*)&( ds402plugin_->getControlWord() ), 's' );
    } 
    else if( moo == coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_VELOCITY_MODE )
    {
      coe_core::ds402::profiled_vel_status_word_t status_word; 
      res.message  += coe_core::ds402::to_string(status_word << *(uint16_t*)&( ds402plugin_->getStatusWord() ), 's' );
      
      coe_core::ds402::profiled_vel_control_word_t control_word; 
      res.message  += coe_core::ds402::to_string(control_word << *(uint16_t*)&( ds402plugin_->getControlWord() ), 's' );
      
    }

    std::cout << res.message << std::endl;
    return (res.success = true);
  }
  
  bool setCoeCommand ( coe_hw_plugins::CoeMessage::Request& req, coe_hw_plugins::CoeMessage::Response& res )
  {
  //
    ROS_INFO ( "%s<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s",BOLDCYAN,RESET);
    ROS_INFO ( "Received the Coe Command '%s', module address: %d", req.message.c_str(), req.addr );
    coe_core::ds402::CommandID      cmd;
    coe_core::ds402::status_word_t  status_word = ds402plugin_->getStatusWord();
    coe_core::ds402::StateID        start_state  = coe_core::ds402::to_stateid ( status_word );
    std::cout << "Actual Status: '"<< BOLDYELLOW << coe_core::ds402::to_string(start_state) << RESET << "'" << std::endl;
    try
    {
      cmd = coe_core::ds402::to_commandid ( req.message );
      std::cout << "Parsed comand: '" << BOLDYELLOW << coe_core::ds402::to_string( cmd) << RESET << std::endl;
    }
    catch ( std::exception& e )
    {
      res.message += "Exception:" + std::string ( e.what() );
      res.message += "----\n";
      res.message += "Req. Command: " + std::string ( req.message ) + "\n";
      res.message +=  coe_core::ds402::echo_feasible_commands ( start_state );
      res.success = false;
      return true;
    }

    try
    {

      if(( coe_core::ds402::get_next_state ( start_state, cmd ) == start_state )
      && ( cmd != coe_core::ds402::CMD_LOOPBACK ) && ( cmd != coe_core::ds402::CMD_SWITCH_ON ) && ( cmd != coe_core::ds402::CMD_COMPLETE_RESET ) )
      {
          res.message  = "Error, the command '" + req.message +"' cannot be applied\n" + coe_core::ds402::echo_feasible_commands ( start_state );
          res.success = true;
      }
      else
      {
        coe_core::ds402::StateID next_state = coe_core::ds402::get_next_state ( start_state, cmd );
        std::cout << "Next Status:   '" << BOLDYELLOW << coe_core::ds402::to_string(next_state) << RESET << std::endl;
        
        coe_core::ds402::control_word_t control_word;
        coe_core::ds402::to_control_word( cmd, (uint16_t*)&control_word );
        std::cout << coe_core::ds402::to_string(control_word,'s') << std::endl;
        ds402plugin_->setControlWord( control_word );

        res.message  = "None";
        res.success = true;
        
        ROS_WARN ( "Wait for the state transition.." );
        coe_core::ds402::StateID act_state; 
        ros::Rate r(10);
        do 
        {
          r.sleep();
          act_state = coe_core::ds402::to_stateid ( ds402plugin_->getStatusWord() );
          if(  act_state == next_state )
            break;;
        }
        while( 1 );
        std::cout << coe_core::ds402::echo_feasible_commands ( act_state  ) <<std::endl;
      }

      ROS_INFO ( "%s>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>%s",BOLDCYAN,RESET);
      return true;
    }
    catch ( std::exception& e )
    {
      res.message  += "Req. Command: " + std::string ( req.message.data() );
      res.message  += "Exception caught :" + std::string ( e.what() );
      res.success = false;
      return true;
    }
  }
  bool setOperationMode ( coe_hw_plugins::CoeMessage::Request& req, coe_hw_plugins::CoeMessage::Response& res )
  {
  //
    ROS_INFO ( "Received the Coe Command '%s%s%s', module address: %s%d%s", BOLDYELLOW,req.message.c_str(), RESET, BOLDYELLOW, req.addr, RESET );
    std::string                       actual_operation_mode = ds402plugin_->getActualState();
    coe_core::ds402::ModeOperationID  actual_moo = coe_core::ds402::to_modeofoperationid( actual_operation_mode );
    ROS_INFO ( "Actual moo '%s%s%s', asked '%s%s%s', module address: %d", BOLDYELLOW,actual_operation_mode.c_str(), RESET, BOLDYELLOW,req.message.c_str(), RESET, req.addr );
    
    auto ns = ds402plugin_->getStateNames();
    
    if( std::find(ns.begin(), ns.end(), req.message) == ns.end() )
    {
      ROS_ERROR ( "Asked mode not available.");
      ROS_INFO  ( "Available modes:");
      for( auto const & s : ns )
        ROS_INFO ( "- %s", s.c_str());
      return false;
    }
    
    try
    {
      res.success = ds402plugin_->setState( req.message ); 
    }
    catch ( std::exception& e )
    {
        res.message += "Exception:" + std::string ( e.what() );
        res.success = false;
        return true;
    }
    return true;
  }
  bool getOperationMode ( std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res )
  {
    res.message = ds402plugin_->getActualState();
    res.success = true; 
    return true;
  }
  
  bool getCoeErrors ( coe_hw_plugins::CoeErrors::Request& req, coe_hw_plugins::CoeErrors::Response& res )
  {
    res.message = ds402plugin_->getErrors();
    for( auto const & m : res.message ) std::cout << m << std::endl;
    return (res.success = true);
  }
  void setCoeTargetPosition(const std_msgs::Float64::ConstPtr& msg)
  {
    *pos_cmd = msg->data;
    *vel_cmd = 0.0;
    *eff_cmd = 0.0;
  }
  void setCoeTargetVelocity(const std_msgs::Float64::ConstPtr& msg)
  {
    *pos_cmd = *pos_msr;
    *vel_cmd = msg->data;
    *eff_cmd = 0.0;
  }
  void setCoeTargetTorque(const std_msgs::Float64::ConstPtr& msg)
  {
    *pos_cmd = *pos_msr;
    *vel_cmd = 0.0;
    *eff_cmd = msg->data;
  }



  bool sync()
  {
    ds402plugin_->read();
    ds402plugin_->write();
    return true;
  }
  
};

int main ( int argc, char* argv[] )
{
    ros::init ( argc,argv,"test_coe_hw_plugins" );
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(4);
    std::string device_coe_parameter = "";

    TestPlugin test( nh );
    
    spinner.start();
    
    ros::ServiceServer coe_command_service_ = nh.advertiseService             ( "set_coe_command",            &TestPlugin::setCoeCommand,         &test );
    ros::ServiceServer coe_config_service_  = nh.advertiseService             ( "set_operation_mode",         &TestPlugin::setOperationMode,      &test );
//     ros::ServiceServer coe_error_service_   = nh.advertiseService ( "get_errors",                 &getErrors, this );
    ros::ServiceServer coe_moo_service_     = nh.advertiseService             ( "get_operation_mode",         &TestPlugin::getOperationMode,      &test);
    ros::ServiceServer coe_status_service_  = nh.advertiseService             ( "get_status_word",            &TestPlugin::getCoeState,           &test);
    ros::ServiceServer coe_errors_service_  = nh.advertiseService             ( "get_errors",                 &TestPlugin::getCoeErrors,          &test);
    ros::Subscriber    coe_target_pos_      = nh.subscribe<std_msgs::Float64> ( "target_position",   1 ,      &TestPlugin::setCoeTargetPosition,  &test);
    ros::Subscriber    coe_target_vel_      = nh.subscribe<std_msgs::Float64> ( "target_velocity",   1 ,      &TestPlugin::setCoeTargetVelocity,  &test);
    ros::Subscriber    coe_target_tau_      = nh.subscribe<std_msgs::Float64> ( "target_torque",     1 ,      &TestPlugin::setCoeTargetTorque,    &test);

    ros::WallRate rt(500);
    int dec = 500;
    int cnt = 0;
    while(ros::ok())
    {
      if( !test.sync() )
        break;
      
      rt.sleep();
    }

    return -1;
}


