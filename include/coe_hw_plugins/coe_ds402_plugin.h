#ifndef __COE_DS402_PLUNGIN_HEADER__H__
#define __COE_DS402_PLUNGIN_HEADER__H__

#include <realtime_tools/realtime_publisher.h>
#include <pluginlib/class_list_macros.h>
#include <std_msgs/Float64MultiArray.h>

#include <itia_futils/string.h>
#include <coe_core/coe_base.h>
#include <coe_core/ds301/coe_bitwise_struct.h>
#include <coe_core/ds301/coe_sdo_dictionary.h>
#include <coe_core/ds402/coe_xfsm_utilities.h>
#include <coe_core/ds402/coe_xfsm_symbols.h>
#include <coe_core/ds402/coe_bitwise_struct.h>

#include <coe_driver/diagnostic/coe_generic_analyzer.h>
#include <coe_driver/hw_plugin/coe_hw_base_plugin.h>


namespace coe_hw_plugins
{

static const std::string DIAGNOSTIC_MSG_NS = "/diagnostic";
  
namespace TxPdoMandatory
{
  const char*      Key         [ 4 ] = { "position", "velocity", "torque", "status_word"   };
  const uint16_t   Index       [ 4 ] = {     0x1A0E,     0x1A0F,   0x1A12,       0x1A0A    };
  enum             ID                  { POSITION=0,   VELOCITY,   TORQUE,   STATUS_WORD   };
}

namespace RxPdoMandatory
{
  const char*      Key         [ 4 ] = { "position", "velocity", "torque", "control_word"  };
  const uint16_t   Index       [ 4 ] = {     0x160F,     0x161C,   0x160C,         0x160A  };
  enum             ID                  { POSITION=0,   VELOCITY,   TORQUE,   CONTROL_WORD  };
}


class DS402Complete : public coe_driver::CoeHwPlugin
{
protected:

  std::mutex                           read_mtx_;
  double                               feedback_position_;
  double                               feedback_velocity_;
  double                               feedback_torque_;
  
  double                               command_position_;
  double                               command_velocity_;
  double                               command_torque_;

  std::map< std::string, double >      analog_inputs_;
  std::map< std::string, double >      analog_outputs_;

  std::map< std::string, bool >        digital_inputs_;
  std::map< std::string, bool >        digital_outputs_;

  coe_core::DataObjectEntry<uint16_t>* status_word_;
  coe_core::DataObjectEntry<uint16_t>* control_word_;
  std::vector< std::string >           state_names_;
  
  
  bool                                 switching_operation_mode_;
  coe_core::ds402::StateID             statusid_prev_;
  coe_core::ds402::ModeOperationID     operation_mode_;
  
  coe_core::ds402::ModeOperationID     getOperationMode  ( );
  bool                                 setOperationMode  ( const coe_core::ds402::ModeOperationID moo, const uint8_t max_num_trials = 50 );
  coe_core::ds402::ModeOperationID     getOperationModeID( );
  
  void  getErrors ( std::vector< std::pair< std::string,std::string> >& errors_map );
  bool  checkErrorsActive( );
  
public:

  DS402Complete( );
  virtual ~DS402Complete( );
  
  bool initialize(ros::NodeHandle& nh, const std::string& device_coe_parameter, int address);
  bool read();
  bool write();
  
  std::vector<std::string>         getStateNames         ( ) const;
  std::string                      getActualState        ( );
  
  //---------------------------------------------------------------------------
  coe_core::ds402::status_word_t   getStatusWord         ( );
  coe_core::ds402::control_word_t  getControlWord        ( );
  uint16_t*                        stateHandle           ( );
  uint16_t*                        controlWordHandle     ( );

  //---------------------------------------------------------------------------
  
  
  std::vector<std::string>         getAnalogInputNames   ( ) const;
  std::vector<std::string>         getAnalogOutputNames  ( ) const;
  
  std::vector<std::string>         getDigitalInputNames  ( ) const;
  std::vector<std::string>         getDigitalOutputNames ( ) const;

  bool  setTargetState            ( const std::string& state_name );
  bool  setControlWord            ( const coe_core::ds402::control_word_t& control_word );
  

  void  jointStateHandle          ( double** pos, double** vel, double** eff );
  void  jointCommandHandle        ( double** pos, double** vel, double** eff );
  
  double* analogInputValueHandle  ( const std::string& name );
  double* analogOutputValueHandle ( const std::string& name );
  
  bool* digitalInputValueHandle   ( const std::string& name );
  bool* digitalOutputValueHandle  ( const std::string& name );
  
  virtual bool hasDigitalInputs   ( ) { return digital_inputs_.size();   };
  virtual bool hasDigitalOutputs  ( ) { return digital_outputs_.size();  };
  virtual bool hasAnalogInputs    ( ) { return analog_inputs_.size();    };
  virtual bool hasAnalogOutputs   ( ) { return analog_outputs_.size();   };
  virtual bool isActuator         ( ) { return true;                     };
  
  std::string dumpTxPdo();
  std::string dumpRxPdo();
  
};





}

#endif