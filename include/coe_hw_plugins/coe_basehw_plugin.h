#ifndef __COE__ROS__H___
#define __COE__ROS__H___

#include <itia_futils/itia_futils.h>

#include <coe_core/coe_utilities.h>
#include <coe_core/ds402/coe_xfsm_utilities.h>

#include <std_msgs/Int32.h>
#include <std_msgs/Int16.h>
#include <std_msgs/String.h>
#include <coe_core/ds301/coe_sdo_dictionary.h>
#include <coe_core/ds402/coe_sdo_dictionary.h>
#include <coe_driver/SetSdo.h>
#include <coe_driver/GetSdo.h>
#include <coe_driver/coe_shared_memory.h>
#include <coe_driver/coe_module_descriptor.h>

static const char* SET_SDO_SERVER_NAMESPACE = "/coe_driver_node/set_sdo";
static const char* GET_SDO_SERVER_NAMESPACE = "/coe_driver_node/get_sdo";

namespace coe_hw_plugins
{
  
  class CoeHwPlugin
  {
  private:

    coe_driver::ModuleSharedMemoryPtr   pdo_shared_memory_;
    
    uint8_t*                            prxpdo_;
    uint8_t*                            prxpdo_previous_;
    
    uint8_t*                            ptxpdo_;
    uint8_t*                            ptxpdo_previous_;
    
    ros::ServiceClient                  set_sdo_;
    ros::ServiceClient                  get_sdo_;
    
  protected:
    
    coe_driver::ModuleDescriptorPtr     module_;

  public:
    
    CoeHwPlugin ( ) { /* nothing to do so far */ }
    
    virtual bool  initialize(ros::NodeHandle& nh, const std::string& device_coe_parameter);
    virtual bool  read();
    virtual bool  write();
    
    virtual bool  readSdo  ( coe_core::BaseDataObjectEntry* in);
    virtual bool  writeSdo ( const coe_core::BaseDataObjectEntry* in);

    virtual const std::vector<std::string>&         getStateNames        ( ) const = 0;
    virtual const std::string&                      getActualState       ( )  = 0;
    virtual const coe_core::ds402::status_word_t&   getStatusWord        ( ) const = 0;
    virtual const coe_core::ds402::control_word_t&  getControlWord       ( ) const = 0;

    virtual const std::string&                      getJointName         ( ) const = 0;

    virtual const std::vector<std::string>&         getAnologInputNames  ( ) const = 0;
    virtual const std::vector<std::string>&         getAnologOutputNames ( ) const = 0;
    
    virtual const std::vector<std::string>&         getDigitalInputNames ( ) const = 0;
    virtual const std::vector<std::string>&         getDigitalOutputNames( ) const = 0;

    virtual const std::vector<std::string>&         getErrors ( ) = 0;
    
    virtual bool  setState( const std::string& state_name ) = 0;
    virtual bool  setControlWord( const coe_core::ds402::control_word_t& control_word ) = 0;
    
    virtual void  getJointStatePtr          (double* pos, double* vel, double* eff) = 0;
    virtual void  getJointCommandPtr        (double*& pos, double*& vel, double*& eff) = 0;
    
    virtual void  getAnalogInputValuePtr    (const std::string& name, double* input ) = 0;
    virtual void  getAnalogOutputValuePtr   (const std::string& name, double* input ) = 0;

    virtual void  getDigitalInputValuePtr   (const std::string& name, bool* input ) = 0;
    virtual void  getDigitalOutputValuePtr  (const std::string& name, bool* input ) = 0;
    
    
  };
  
  
}


    
    
    
    
    
#endif